#!/bin/bash
# @author Guillaume VILLENA
# @version 1.0.0
# @date 20/12/2018
# @filedescription A bash script that generate documentation in markdown format for a list of specified files
#   --create-summary : create a summary markdown of all found functions and files
#   --file=script.sh,script2, : file to analyse
#   --output-dir=/my/dir : the destination directory for generated files
#   --create-output-dir : if set the output directory will be created
#   --input-dir=dir/ : directory to generate documentation from
# Examples:
# bashdoc.sh --create-sumary --file=myscript.sh
# bashdoc.sh --file=myscript.sh --create-output-dir
#



# colors
RED="\e[31m"
RESET="\e[0m"

create_summary=false
create_output_dir=false
output_dir=.
input_files=
in_txt_bloc=false
bloc_type=""
functionList=""
input_dir=
singlefile=false

for i in "$@"
do
    case $i in
        --create-summary)
            create_summary=true
        ;;
        --output-dir=*)
            output_dir="${i#*=}"
        ;;
        --file=*)
            input_files="${i#*=}"
        ;;
        --create-output-dir)
            create_output_dir=true
        ;;
        --input-dir=*)
            input_dir="${i#*=}"
        ;;
        --single-file)
            singlefile=true
        ;;
    esac
done

#Everything is ok

if $create_output_dir
then
    mkdir -p "$output_dir"
fi

if [ ! -d "$output_dir" ]
then
    echo -e "$RED Output directory is not a directory or not found ! $RESET"
    exit 1
fi

if [[ -n $input_dir ]]
then
    foundFiles=$(find $input_dir -type f -not -path '*/\.*')
    input_files="$input_files,$(echo $foundFiles | tr ' ' ',')"
fi

for file in $( echo $input_files | tr ',' '\n' )
do

    if [ ! -f $file ]
    then
        echo -e "$RED the file $file cannot be opened $RESET"
    fi

    contentTmpFile=$(mktemp)
    tableOfContent=$(mktemp)
    headerFile=$(mktemp)

    i=1
    echo "Current file is $file"
    echo "# $file" >> "$headerFile"
    while read -r line
    do
        # echo $line
        if ! $in_txt_bloc
        then
            if [[ $line =~ .*@author.* ]]
            then
                echo "**Author** : $( echo $line | awk -F'@author' '{print $2}')" >> "$headerFile"

            elif [[ $line =~ .*@version.* ]]
            then
                echo "**Version** : $( echo $line | awk -F'@version' '{print $2}')" >> "$headerFile"

            elif [[ $line =~ .*@license.* ]]
            then
                echo "**Licensed under** : $( echo $line | awk -F'@license' '{print $2}')" >> "$headerFile"

            elif [[ $line =~ .*@date.* ]]
            then
                echo "**Creation date** : $( echo $line | awk -F'@date' '{print $2}')" >> "$headerFile"

            elif [[ $line =~ .*@description.* ]]
            then
                in_txt_bloc=true
                bloc_type="description"
                current_file=$contentTmpFile
                echo "**Description** : " >> $current_file
                echo "$( echo $line | awk -F'@description' '{print $2}')" >> $contentTmpFile

            elif [[ $line =~ .*@filedescription.* ]]
            then
                in_txt_bloc=true
                bloc_type="description"
                current_file=$headerFile
                echo "**Description** : " >> $current_file
                echo "" >> $current_file
                echo "$( echo $line | awk -F'@filedescription' '{print $2}')" >> "$current_file"

            elif [[ $line =~ .*@args.* ]]
            then
                in_txt_bloc=true
                bloc_type="args"
                current_file=$contentTmpFile
                echo "**Arguments** : " >> "$contentTmpFile"


            elif [[ $line =~ .*@example.* ]]
            then
                in_txt_bloc=true
                bloc_type="example"
                current_file=$contentTmpFile
                echo "**Examples** : " >> "$contentTmpFile"

            elif [[ $line =~ .*@return.* ]]
            then
                echo "**Return** : " >> "$contentTmpFile"
                echo " * $( echo $line | awk -F'@return' '{print $2}')" >> "$contentTmpFile"
                in_txt_bloc=true
                bloc_type=return
                current_file=$contentTmpFile

            elif [[ $line =~ .*@note.* ]]
            then
                in_txt_bloc=true
                bloc_type="note"
                current_file=$contentTmpFile
                echo "**Note** : " >> "$contentTmpFile"
                echo "$( echo $line | awk -F'@note' '{print $2}')" >> "$contentTmpFile"

            elif [[ $line =~ .*@stdout.* ]]
            then
                echo "**Stdout** : $( echo $line | awk -F'@stdout' '{print $2}')" >> "$contentTmpFile"
                echo "" >> "$contentTmpFile"

            elif [[ $line =~ .*@stderr.* ]]
            then
                echo "**Stderr** : $( echo $line | awk -F'@stderr' '{print $2}')" >> "$contentTmpFile"
                echo "" >> "$contentTmpFile"
            elif [[ $line =~ .*@noargs.* ]]
            then

                echo "**Arguments** : This function has no arguments" >> "$contentTmpFile"
                echo "" >> "$contentTmpFile"
            elif [[ $line =~ .*@see.* ]]
            then
                func=$( echo $line | awk -F'@see' '{print $2}')
                echo "**See** : [$func](#$(echo $func | tr -d ' '))" >> "$contentTmpFile"
                echo "" >> "$contentTmpFile"

            elif [[ $line =~ ^([0-9]*:)[^=#\ ]+(\ )?\(\)\ ?{? ]]
            then
                echo "" >> "$contentTmpFile"
                echo "" >> "$contentTmpFile"
                funcName=$(echo ${line//[[:blank:]]/} | awk -F':' '{print $2}' |tr '{' ' ' )
                echo "## $funcName <a name=\"$(echo $funcName | tr -d ' ')\"></a>" >> "$contentTmpFile"
                echo "" >> "$contentTmpFile"
                functionList="$functionList $funcName"

            elif [[ $line =~ .*@alias.* ]]
            then
                alias_found=true
                echo -e "## --+ALIAS+-- \n" >> "$contentTmpFile"

            elif [[ $line =~ ^([0-9]*:)[[:space:]]*alias\ .+= ]]
            then
                alias_found=false
                # Get the alias name
                alias_name=$(echo $line | awk -F':' '{print $2}' | sed -re 's|^[[:space:]]*alias[[:space:]]+([^=]+)=.*$|\1()|gm' | tr -d ' ' )
                # Replace the place holder and put the link
                sed -i "s|--+ALIAS+--|$alias_name <a name=\"$alias_name\"></a>|" $contentTmpFile
                # Add alias to the function list
                functionList="$functionList $alias_name"
            fi

        elif [[ $line =~ ^([0-9]*:[[:space:]]*#)(\ *)$ ]]
        then
            in_txt_bloc=false
            echo "" >> "$current_file"
            echo "" >> "$current_file"
        else
            case $bloc_type in
                description)
                    echo "$(echo "$line" | sed -r 's|^([0-9]*:[[:space:]]*#)(.*)$|\2|gm')" >> "$current_file"
                ;;
                args)
                    echo "$line" | sed -re 's/^[0-9]*:[[:space:]]*#\ *([a-zA-Z0-9\$\.\,\/\\=\*\@\_\#\-]*)\ *:\ *(.*)/ \* \1 : \2/gm' >> "$current_file"
                ;;
                example)
                    echo " - $(echo "$line" | sed -r 's|^([0-9]*:[[:space:]]*#)\ *(.*)$|\2|gm')" >> "$current_file"
                ;;
                return)
                    echo " * $(echo "$line" | sed -r 's|^([0-9]*:[[:space:]]*#)\ *(.*)$|\2|gm')" >> "$current_file"
                ;;
                note)
                    echo "$(echo "$line" | sed -r 's|^([0-9]*:[[:space:]]*#)(.*)$|\2|gm')" >> "$current_file"
                ;;
            esac
            echo "" >> "$current_file"
        fi

        #echo $i $line
        ((i=i+1))
    done < <(cat <(grep -nE '^[[:space:]]*#' $file) <(grep -noE '^[^=#\ ]+(\ )?\(\)\ ?{?' $file) <( grep -nE "^[[:space:]]*alias .+=" $file) | sort -n)


    # Append Table of content
    echo "" >> "$tableOfContent"
    echo "Table of Content" >> "$tableOfContent"
    for fn in $functionList
    do
        echo "- [$fn](#$fn)" >> "$tableOfContent"
    done

    mkdir -p $(dirname "$output_dir/$file.doc.md")
    cat $headerFile $tableOfContent $contentTmpFile > "$output_dir/$file.doc.md"

    rm $headerFile
    rm $tableOfContent
    rm $contentTmpFile

    functionList=""
    fileList="$file $fileList"
done

if $singlefile
then
    singleFilecontentTMP=$(mktemp)
fi

if $create_summary
then
    echo "Creating index file"
    # Append Table of content for all file
    echo "" > "$output_dir/index.doc.md"
    echo "Table of Content" >> "$output_dir/index.doc.md"
    for fl in $fileList
    do
        echo "- [$fl](#$fl)" >> "$output_dir/index.doc.md"

        # collect the table of content of others so you can see function of each file in the index
        #grep -E "^\- \[.*\]\(#.*\)\)" "$output_dir/$fl.doc.md" | perl -ne 'print "    $_"' | sed -re "s|(#.*)|$fl\1|gm" >> "$output_dir/index.doc.md"
        grep -E "^\- \[.*\]\(#.*\)\)" "$output_dir/$fl.doc.md" | perl -ne 'print "    $_"'  >> "$output_dir/index.doc.md"

        if $singlefile
        then
            echo "# $fl <a name=\"$(echo $fl | tr -d ' ')\"></a>" >> "$singleFilecontentTMP"
            echo "" >> "$singleFilecontentTMP"
            grep -m 1 -n "Table of Content" "$output_dir/$fl.doc.md" | cut -d ':' -f 1 | { read number; echo $((number-1)); } | xargs -I % head -n +%  "$output_dir/$fl.doc.md" | tail -n +2 >> "$singleFilecontentTMP"
            #grep -m 1 -n "##" "$output_dir/$fl.doc.md" | cut -d ':' -f 1
            grep -m 1 -n "##" "$output_dir/$fl.doc.md" | cut -d ':' -f 1 | xargs -I % tail -n +% "$output_dir/$fl.doc.md" >> "$singleFilecontentTMP"
        fi
    done
fi

if $singlefile
then
    cat  "$output_dir/index.doc.md" "$singleFilecontentTMP" > "$output_dir/index.full.doc.md"
    rm $singleFilecontentTMP

    for fl in $fileList
    do
        rm "$output_dir/$fl.doc.md"
    done

    find "$output_dir" -type d -empty -delete
fi

myfunction(){

# @description A simple function that print toto
#
# @args
#   $1 : number 1
#   $@ : the rest
#
# @return 0 on success
#       >0 on error
#
# @stdout many things


    echo "toto"
}

myfunction2(){

# @description A simple function2 that print toto
#
# @args
#   $1 : number 1
#   $@ : the rest
#
# @return 0 on success
#       >0 on error
#
# @stdout many things


    echo "toto2"
}

# @alias
# @description alias from egrep to grep -E
# See grep manual for more info
#
# @args
# $@ : grep arguments
#
# @stdout grep stdout
# @stderr grep stderr
# @return grep return code
#
alias egrep="grep -E"